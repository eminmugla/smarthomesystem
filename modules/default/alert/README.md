# Module: Alert

The alert module is one of the default modules of the Smart GYM Home. This module displays notifications from other modules.

For configuration options, please check the [SmartHomeGym documentation](https://docs.magicmirror.builders/modules/alert.html).
